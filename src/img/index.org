#+title:  Images used in heap sort experiment
#+AUTHOR: VLEAD
#+DATE: [2018-05-14 Mon]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
 Here are the required images used for our project:


|--------+---------------------------------+-----------------------|
| *SNo.* | *Purpose*                       | *Link*                |
|--------+---------------------------------+-----------------------|
|      1 | Image for extract max operation | [[./extractMax.png][extract max operation]] |
|--------+---------------------------------+-----------------------|
|      2 | Image for insert operation      | [[./insert.png][insert operation]]      |
|--------+---------------------------------+-----------------------|
|      3 | Image for rebuild operation     | [[./rebuild.png][rebuild operation]]     |
|--------+---------------------------------+-----------------------|
|      4 | Min heap and max heap image     | [[./minMax.png][minheap and maxheap]]   |
|--------+---------------------------------+-----------------------|
